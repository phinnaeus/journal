TAG_COMMIT := $(shell git rev-list --abbrev-commit --tags --max-count=1)
TAG := $(shell git describe --abbrev=0 --tags ${TAG_COMMIT} 2>/dev/null)
COMMIT := $(shell git rev-parse --short HEAD)
DATE := $(shell git log -1 --format=%cd --date=unix)
VERSION := $(TAG)
ifneq ($(strip $(COMMIT)),$(strip $(TAG_COMMIT)))
	VERSION := $(VERSION)-$(DATE)+sha.$(COMMIT)
endif

# during yarn builds on CI, go is not available
ifndef CI_YARN_BUILD
PACKAGES := $(shell go list ./...)
endif
OUTPUT_DIR := .
# the nomsgpack tag is from the go gin-gonic library
GO_BUILD_FLAGS := -tags=nomsgpack -ldflags="-X 'phinnae.us/journal/cmd.version=$(VERSION)'"

.PHONY: all release build yarn test cover vet clean
all: vet test build

# locally we want to run the yarn build when we run a normal build, but in CI it's separate
ifndef REPO_NAME
build: yarn
endif
build:
	@echo "building version $(VERSION)"
	go build -o $(OUTPUT_DIR)/ $(GO_BUILD_FLAGS) phinnae.us/journal

release: build
ifdef GOOS
	mkdir -p $(OUTPUT_DIR)/${GOOS}_${GOARCH}
	mv $(OUTPUT_DIR)/journal $(OUTPUT_DIR)/${GOOS}_${GOARCH}/journal
else
	@echo "skipping release when \$$GOOS is undefined"
endif

ui/dist/index.js: $(wildcard ui/js/*.tsx)
	yarn tsc -p tsconfig.json
	yarn esbuild ui/js/index.tsx --bundle --preserve-symlinks --minify --sourcemap --outfile=ui/dist/index.js

# this doesn't appear to be working correctly
ui/dist/index.css: $(wildcard ui/css/*.scss)
	yarn sass ui/css/index.scss --style=compressed ui/dist/index.css
	ls -lh ui/dist/index.css

yarn: ui/dist/index.js ui/dist/index.css

test:
	go run gotest.tools/gotestsum --format testname -- -race $(PACKAGES)

cover:
	# -covermode must be "atomic", not "count", when -race is enabled
	go run gotest.tools/gotestsum --junitfile unit-tests.xml -- -coverprofile=coverage.out -covermode count $(PACKAGES)
	# this gives us the overall coverage
	# gitlab uses this regex to get the output: /total:\s+\(statements\)\s+(\d+.\d+)%/
	go tool cover -func coverage.out
	# this gives us a line by line cobertura report
	go run github.com/boumenot/gocover-cobertura < coverage.out > coverage.xml

vet:
	go fmt $(PACKAGES)
	go vet $(PACKAGES)

clean:
	rm -vf ui/dist/*
	touch ui/dist/embed-generated
	rm -vf $(OUTPUT_DIR)/journal
