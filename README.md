# Journal

Journal is a CLI to help you keep track of the things you've done. You can
think of it as a daily log coupled with a TODO list. There is a free-form
markdown entry area plus extra metadata for each day such as what new TODOs
were created, any TODOs that were completed, and any terminal commands that
were worthy of note.

***Journal is under active development and all features are subject to change.***

## Development
Because of the embedded web UI, `journal` is not a pure Go application. All of
the non Go stuff should be contained within the `ui` package however so if you
don't need to edit that part of the application all you'll need is Go tooling.

### Prerequisites
* Make
* Go (at least version 1.17)
* Node.js (at least version 16)
* Yarn 2

The first time you check out the repository you should install the necessary
dependencies:

```shell
go mod download && yarn install
````

### Normal process
All the necessary commands for development are contained within the Makefile.
For normal development just run `make` and let it do the needful. That will
output a `journal` binary in the root directory you can run and test.

# Credits

* Repository icon made by [Smashicons](https://www.flaticon.com/authors/smashicons) from [www.flaticon.com](https://www.flaticon.com)
