package cmd

import (
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"io"
	"os"
	"path/filepath"
	"phinnae.us/journal/database"
	"strings"
)

func init() {
	rootCmd.AddCommand(commandCommand)
}

var commandCommand = &cobra.Command{
	Use:   "command [search]",
	Args:  cobra.MaximumNArgs(1),
	Short: "Add a recent command to today's Journal entry",
	Long: `Sometimes you run a command that is worth remembering. This saves a
command from your terminal history to the daily entry metadata. If the search
term is omitted, Journal adds the previous command.

Note that this has only been tested with 'zsh' and for proper function, this
requires some specific '.zshrc' options.
	HISTFILE=~/.zsh_history
	setopt INC_APPEND_HISTORY_TIME

You may also want to increase the size of your history:
	HISTSIZE=10000
	SAVEHIST=10000`,
	RunE: func(cmd *cobra.Command, args []string) error {
		db, ok := database.FromContext(cmd.Context())
		if !ok {
			return errors.New("unable to load database from context")
		}

		histFile := os.Getenv("HISTFILE")
		if histFile == "" {
			homeDir := os.Getenv("HOME")
			if homeDir != "" {
				// TODO fallbacks
				histFile = filepath.Join(homeDir, ".zsh_history")
			}
		}
		history, err := readLastNLines(histFile, 20)
		if err != nil {
			return fmt.Errorf("unable to read from $HISTFILE: %q: %s", histFile, err)
		}
		if len(history) == 0 {
			return errors.New("no history found")
		}
		log.Debugf("read the last %d lines of shell history", len(history))
		var matches []string
		if len(args) > 0 {
			query := args[0]
			fmt.Printf("Using query: %q\n", query)
			matches = searchHistory(history, query, false)
		} else {
			matches = searchHistory(history, "", true)
		}

		for _, match := range matches {
			if db.Current.AddCommand(match) {
				fmt.Printf("Added command: %q\n", match)
			} else {
				log.Debugf("command %q already present", match)
			}
		}

		return nil
	},
}

func searchHistory(history []string, query string, first bool) []string {
	var result []string
	for _, cmd := range history {
		// TODO is this the most durable way to detect and clean the initial timestamp?
		if strings.HasPrefix(cmd, ": ") {
			cmd = strings.SplitN(cmd, ";", 2)[1]
		}

		// skip journal commands
		if strings.HasPrefix(cmd, "journal") {
			continue
		}

		if strings.Contains(cmd, query) {
			result = append(result, cmd)
			if first {
				return result
			}
		}
	}
	return result
}

func readLastNLines(filename string, numLines int) ([]string, error) {
	var lines []string

	fi, err := os.Stat(filename)
	if err != nil {
		return lines, err
	}

	file, err := os.Open(filename)
	if err != nil {
		return lines, err
	}
	defer file.Close()

	var cursor int64 = -1
	for len(lines) < numLines {
		line := ""
		for {
			cursor -= 1
			file.Seek(cursor, io.SeekEnd)

			char := make([]byte, 1)
			file.Read(char)

			if cursor != -1 && (char[0] == '\n' || char[0] == '\r') {
				// stop if we find a line
				lines = append(lines, line)
				break
			} else {
				//line = fmt.Sprintf("%s%s", string(char), line) // there is more efficient way
				line = string(char) + line
			}

			if cursor == -fi.Size() { // stop if we are at the beginning
				break
			}
		}
	}

	return lines, nil
}
