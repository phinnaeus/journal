package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"os/exec"
	"phinnae.us/journal/database"
)

func init() {
	rootCmd.AddCommand(entryCommand)
}

var entryCommand = &cobra.Command{
	Use:   "entry",
	Args:  cobra.NoArgs,
	Short: "Open today's Journal entry in your default Markdown editor",
	RunE: func(cmd *cobra.Command, args []string) error {
		db, ok := database.FromContext(cmd.Context())
		if !ok {
			return errors.New("unable to load database from context")
		}

		if len(db.Todos) > 0 {
			fmt.Printf("You have %d outstanding todos", len(db.Todos))
			for i, t := range db.Todos {
				fmt.Printf("%d: %s\n", i, t)
			}
		}

		path := db.CurrentContentPath()
		f, err := os.OpenFile(path, os.O_CREATE, os.FileMode(0640))
		if err != nil {
			return err
		}
		err = f.Close()
		if err != nil {
			return err
		}

		// TODO mac only
		open := exec.Command("/usr/bin/open", path)
		if err := open.Run(); err != nil {
			return err
		}

		return nil
	},
}
