package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"phinnae.us/journal/config"
)

func init() {
	rootCmd.AddCommand(initCommand)
}

var initCommand = &cobra.Command{
	Use:   "init [journal-root]",
	Args:  cobra.MaximumNArgs(1),
	Short: "Initialize user-level Journal configuration",
	RunE: func(cmd *cobra.Command, args []string) error {
		var path string
		if len(args) >= 2 {
			path = args[1]
		}

		_, err := config.InitConfig(path)
		if err != nil {
			return err
		}

		fmt.Println("Done!")
		return nil
	},
	PersistentPostRunE: noDatabaseSave,
}
