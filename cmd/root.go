package cmd

import (
	"context"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	flag "github.com/spf13/pflag"
	"golang.org/x/mod/semver"
	"io"
	"os"
	"phinnae.us/journal/config"
	"phinnae.us/journal/database"
)

func init() {
	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "output debug logs")
}

var (
	verbose bool
	version = "v0.0.0-fix.this.if.you.see.it"
	rootCmd = &cobra.Command{
		Use:   "journal",
		Short: "Daily log tool",
		Long: `Journal is a tool to keep track of things you do. It is inspired
by engineers at a previous job who kept detailed Wikis of all their own work on
a day to day or week to week basis.`,
		Version: version,
		PersistentPostRunE: func(cmd *cobra.Command, args []string) error {
			db, ok := database.FromContext(cmd.Context())
			if ok {
				log.Debug("Saving DB")
				return db.SaveCurrent(semver.Major(version))
			} else {
				log.Warn("Unable to pull database from context")
			}
			return nil
		},
	}
	noDatabaseSave = func(cmd *cobra.Command, args []string) error {
		log.Debugf("not saving DB in %q command", cmd.Use)
		return nil
	}
)

// Execute executes the root command.
func Execute() error {
	// verbose flag parsing is done here so we can get earlier debug logs
	fs := flag.NewFlagSet("verbose", flag.ContinueOnError)
	fs.AddFlagSet(rootCmd.PersistentFlags())
	_ = fs.Parse(os.Args[1:])

	if err := setUpLogs(os.Stdout, verbose); err != nil {
		return err
	}
	log.Debug("Logger setup complete.")

	cfg, err := config.ReadConfig()
	if err != nil {
		return err
	}

	db, err := database.Load(cfg.JournalRoot, semver.Major(version))
	if err != nil {
		return err
	}

	ctx := database.NewContext(context.Background(), &db)

	return rootCmd.ExecuteContext(ctx)
}

func setUpLogs(out io.Writer, verbose bool) error {
	log.SetOutput(out)
	if verbose {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}
	return nil
}
