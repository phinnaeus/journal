package cmd

import (
	"errors"
	"github.com/spf13/cobra"
	"phinnae.us/journal/database"
)

func init() {
	rootCmd.AddCommand(rotateCommand)
}

var rotateCommand = &cobra.Command{
	Use:   "rotate",
	Short: "Manually rotate your current entry",
	Long: `By default Journal will automatically rotate your current entry
into the database after 24 hours. This command allows you to trigger a manual
rotation as long as there's not already a rotated entry for today.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		db, ok := database.FromContext(cmd.Context())
		if !ok {
			return errors.New("unable to load database from context")
		}

		return db.Rotate()
	},
}
