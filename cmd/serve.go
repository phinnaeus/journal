package cmd

import (
	"errors"
	"github.com/spf13/cobra"
	"phinnae.us/journal/database"
	"phinnae.us/journal/ui"
)

func init() {
	rootCmd.AddCommand(serverCommand)
}

var serverCommand = &cobra.Command{
	Use:   "serve",
	Args:  cobra.NoArgs,
	Short: "Run a local web server for browsing historical Journal entries",
	RunE: func(cmd *cobra.Command, args []string) error {
		db, ok := database.FromContext(cmd.Context())
		if !ok {
			return errors.New("unable to load database from context")
		}

		return ui.Serve(*db)
	},
	PersistentPostRunE: noDatabaseSave,
}
