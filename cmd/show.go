package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"io"
	"os"
	"phinnae.us/journal/database"
	iio "phinnae.us/journal/io"
)

func init() {
	rootCmd.AddCommand(showCommand)
}

var showCommand = &cobra.Command{
	Use:   "show",
	Args:  cobra.NoArgs,
	Short: "Display the current Journal entry, content, and to-do list",
	RunE: func(cmd *cobra.Command, args []string) error {
		db, ok := database.FromContext(cmd.Context())
		if !ok {
			return errors.New("unable to load database from context")
		}

		stats, err := db.Stats()
		if err != nil {
			return err
		}

		fmt.Printf("Total Entries: %d\nTotal Content Entries: %d\n\n", stats.NumEntries, stats.NumContent)

		fmt.Printf("Current Entry: #%d\nDate: %s\n", db.Current.Ordinal, db.Current.Created.String())

		if iio.FileExists(db.CurrentContentPath()) {
			f, err := os.Open(db.CurrentContentPath())
			if err != nil {
				return err
			}
			defer f.Close()

			fmt.Println("Content:")
			_, err = io.Copy(os.Stdout, f)
			if err != nil {
				return err
			}
			fmt.Println()
		} else {
			fmt.Printf("No Current Entry Content\n\n")
		}

		fmt.Println("TODOs:")
		for i, t := range db.Todos {
			fmt.Printf("%d: %s\n", i, t)
		}

		return nil
	},
}
