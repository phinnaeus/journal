package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"phinnae.us/journal/database"
	"strconv"
)

func init() {
	rootCmd.AddCommand(todoCommand)
	todoCommand.AddCommand(doneCommand)
	todoCommand.AddCommand(deleteCommand)
}

var todoCommand = &cobra.Command{
	Use:   "todo [to-do id|new todo content]",
	Args:  cobra.MaximumNArgs(1),
	Short: "Manage your to-do list",
	RunE: func(cmd *cobra.Command, args []string) error {
		db, ok := database.FromContext(cmd.Context())
		if !ok {
			return errors.New("unable to load database from context")
		}

		if len(args) == 0 {
			for i, t := range db.Todos {
				fmt.Printf("%d: %s\n", i, t)
			}

			return nil
		}

		id, err := strconv.ParseInt(args[0], 10, 64)
		if err != nil || id < 0 {
			// create a new to-do
			db.Todos = append(db.Todos, args[0])
			db.Current.NewTodos = append(db.Current.NewTodos, args[0])
		} else {
			// display an existing to-do
			fmt.Println(db.Todos[id])
		}

		return nil
	},
}

var doneCommand = &cobra.Command{
	Use:   "done todo-id",
	Args:  cobra.ExactValidArgs(1),
	Short: "Complete a to-do",
	Long: `Remove a to-do from your active to-dos and append it to today's'
"completed to-do" list.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("specify the ID of the TODO to complete")
		}
		id, err := strconv.ParseInt(args[0], 10, 64)
		if err != nil || id < 0 {
			return errors.New("id must be a postive integer")
		}

		db, ok := database.FromContext(cmd.Context())
		if !ok {
			return errors.New("unable to load database from context")
		}

		return db.CompleteTodo(int(id))
	},
}

var deleteCommand = &cobra.Command{
	Use:   "delete to-do-id",
	Args:  cobra.ExactValidArgs(1),
	Short: "Delete a to-do",
	Long: `Remove a to-do from your active to-dos without appending it to
today's "completed to-do" list. Also attempts to remove it from the today's
"new to-do" list, but if it was added before today then this will fail.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("specify the ID of the TODO to complete")
		}
		id, err := strconv.ParseInt(args[0], 10, 64)
		if err != nil || id < 0 {
			return errors.New("id must be a postive integer")
		}

		db, ok := database.FromContext(cmd.Context())
		if !ok {
			return errors.New("unable to load database from context")
		}

		return db.DeleteTodo(int(id))
	},
}
