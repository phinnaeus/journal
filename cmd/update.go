package cmd

import (
	"github.com/spf13/cobra"
	"phinnae.us/journal/updater"
)

func init() {
	updateCommand.Flags().BoolP("force", "f", false, "force update")
	rootCmd.AddCommand(updateCommand)
}

var updateCommand = &cobra.Command{
	Use:   "update [explicit version]",
	Args:  cobra.MaximumNArgs(1),
	Short: "Update Journal from the offical GitLab repository",
	RunE: func(cmd *cobra.Command, args []string) error {
		var explicit string
		if len(args) > 0 {
			explicit = args[0]
		}

		force, err := cmd.Flags().GetBool("force")
		if err != nil {
			return err
		}

		return updater.Update(version, force, explicit)
	},
	PersistentPostRunE: noDatabaseSave,
}
