package config

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"path/filepath"
	"phinnae.us/journal/io"

	"github.com/manifoldco/promptui"
)

var (
	baseConfigDir string
	userHomeDir   string
	configFile    string
)

func init() {
	userConfigDir, err := os.UserConfigDir()
	if err != nil {
		panic(err)
	}

	baseConfigDir = filepath.Join(userConfigDir, "journal")
	configFile = filepath.Join(baseConfigDir, "config.json")

	userHomeDir, err = os.UserHomeDir()
	if err != nil {
		panic(err)
	}
}

type Config struct {
	JournalRoot string
}

func InitConfig(path string) (Config, error) {
	if io.FileExists(baseConfigDir) {
		currentCfg, err := ReadConfig()
		if err != nil {
			log.WithError(err).Warn("Configuration already exists, but unable to read it")
		} else {
			log.Warnf("Configuration already exists. Current config: %+v", currentCfg)
		}
	}

	err := os.MkdirAll(baseConfigDir, os.FileMode(740))
	if err != nil {
		return Config{}, err
	}

	if path == "" {
		rootPrompt := promptui.Prompt{
			Label:   "Journal root",
			Default: filepath.Join(userHomeDir, "journal"),
			Validate: func(s string) error {
				return nil
			},
		}
		path, err = rootPrompt.Run()
		if err != nil {
			return Config{}, err
		}
	}

	err = os.MkdirAll(path, os.FileMode(0740))
	if err != nil {
		return Config{}, err
	}

	config := &Config{JournalRoot: path}
	blankConfigBytes, err := json.MarshalIndent(config, "", "  ")
	if err != nil {
		return *config, err
	}

	return *config, os.WriteFile(configFile, blankConfigBytes, os.FileMode(0640))
}

func ReadConfig() (Config, error) {
	// TODO this filename is too common
	overrideCfg, err := loadConfig("config.json")
	if err == nil {
		return overrideCfg, nil
	} else if !os.IsNotExist(err) {
		fmt.Println("WARN: problem reading local config.json file:", err)
	}

	return loadConfig(configFile)
}

func loadConfig(file string) (Config, error) {
	var cfg Config
	bytes, err := os.ReadFile(file)
	if err != nil {
		return cfg, err
	}

	err = json.Unmarshal(bytes, &cfg)
	return cfg, err
}
