package database

import "context"

type key int

var dbKey key

func NewContext(ctx context.Context, db *Database) context.Context {
	return context.WithValue(ctx, dbKey, db)
}

func FromContext(ctx context.Context) (*Database, bool) {
	db, ok := ctx.Value(dbKey).(*Database)
	return db, ok
}
