package database

import (
	"context"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestFromContext(t *testing.T) {
	db := Database{
		RootDir: "test",
	}

	ctx := NewContext(context.Background(), &db)

	actualDb, ok := FromContext(ctx)
	require.True(t, ok)
	require.Equal(t, db.RootDir, actualDb.RootDir)
}
