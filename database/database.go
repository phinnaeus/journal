package database

import (
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/fs"
	"os"
	"path/filepath"
	"phinnae.us/journal/io"
	"strconv"
	"time"
)

const (
	currentEntry   = "_current.json"
	savedEntry     = "entry.json"
	savedContent   = "content.md"
	currentContent = "_current.md"
	currentTodos   = "todos.json"
	versionFile    = "_version"
)

type Database struct {
	RootDir string
	Current *Entry
	Todos   []string
}

func Load(rootDir string, version string) (Database, error) {
	fi, err := os.Stat(rootDir)
	if err != nil {
		return Database{}, err
	}

	if !fi.IsDir() {
		return Database{}, fmt.Errorf("%q is not a directory", rootDir)
	}

	var savedVersion string
	vBytes, err := os.ReadFile(filepath.Join(rootDir, versionFile))
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			savedVersion = "v0"
		} else {
			return Database{}, err
		}
	} else {
		savedVersion = string(vBytes)
	}
	log.Debugf("Saved DB found with version %q", savedVersion)

	err = migrate(savedVersion, version)
	if err != nil {
		return Database{}, err
	}

	current, err := loadEntry(filepath.Join(rootDir, currentEntry))
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			log.Debug("No current entry found, starting a new journal from ordinal 1.")
			current = NewEntry(1)
		} else {
			return Database{}, err
		}
	}
	log.Debugf("Current entry #%d loaded for date %s", current.Ordinal, current.Created.String())

	todos, err := loadTodos(filepath.Join(rootDir, currentTodos))
	if err != nil {
		return Database{}, err
	}
	log.Debugf("%d Todos loaded", len(todos))

	db := Database{
		RootDir: rootDir,
		Current: current,
		Todos:   todos,
	}

	cutoff := time.Now().Add(-24 * time.Hour)
	if current.Created.Before(cutoff) {
		log.Debugf("Auto-rotating current entry which is too old (created: %s)", current.Created.String())
		err = db.Rotate()
		if err != nil {
			return db, err
		}
	}

	return db, nil
}

// LoadEntry note that the entryDate parameter only needs to be the correct day
func (d Database) LoadEntry(entryDate time.Time) (*Entry, error) {
	// TODO cache
	return loadEntry(filepath.Join(entryPath(d.RootDir, entryDate), savedEntry))
}

func (d Database) LoadContent(entryDate time.Time) ([]byte, error) {
	// TODO cache
	return os.ReadFile(filepath.Join(entryPath(d.RootDir, entryDate), savedContent))
}

func (d Database) SaveCurrent(version string) error {
	err := os.WriteFile(filepath.Join(d.RootDir, versionFile), []byte(version), fs.FileMode(0640))
	if err != nil {
		return err
	}

	err = d.saveCurrentTodos()
	if err != nil {
		return err
	}

	return d.Current.Write(filepath.Join(d.RootDir, currentEntry))
}

func (d Database) CurrentContentPath() string {
	return filepath.Join(d.RootDir, currentContent)
}

func (d *Database) Rotate() error {
	basePath := entryPath(d.RootDir, d.Current.Created)
	err := os.MkdirAll(basePath, 0744)
	if err != nil {
		return err
	}

	entryPath := filepath.Join(basePath, savedEntry)
	if io.FileExists(entryPath) {
		log.Warnf("An entry has already been rotated today: %s", entryPath)
		tempDir, err := os.MkdirTemp(basePath, "duplicate_*")
		if err != nil {
			panic(fmt.Errorf("unable to create temporary directory for duplicate rotate situation: %w", err))
		}
		log.Warnf("Please reconcile duplicate entry data under %s", tempDir)
		entryPath = filepath.Join(tempDir, savedEntry)
	}

	err = d.Current.Write(entryPath)
	if err != nil {
		return err
	}

	// TODO locking? probably not needed
	// update: definitely needed
	if io.FileExists(d.CurrentContentPath()) {
		err = os.Rename(d.CurrentContentPath(), filepath.Join(basePath, savedContent))
		if err != nil {
			return err
		}
	}

	d.Current = NewEntry(d.Current.Ordinal + 1)
	log.Infof("Journal rotation complete, new entry is #%d", d.Current.Ordinal)
	return nil
}

func entryPath(rootDir string, entryDate time.Time) string {
	yearPath := strconv.Itoa(entryDate.Year())
	monthPath := fmt.Sprintf("%02d", int(entryDate.Month()))
	dayPath := fmt.Sprintf("%02d", entryDate.Day())
	return filepath.Join(rootDir, yearPath, monthPath, dayPath)
}
