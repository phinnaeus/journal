package database

import (
	"encoding/json"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestDatabase_CurrentContentPath(t *testing.T) {
	root := t.TempDir()

	db, err := Load(root, "v0")
	require.NoError(t, err)

	require.Equal(t, filepath.Join(root, currentContent), db.CurrentContentPath())
}

func TestLoad_Entry_Current(t *testing.T) {
	root := t.TempDir()
	entryTime := time.Now().Add(-1 * time.Hour)

	entry := Entry{
		Ordinal: 3,
		Created: entryTime,
	}
	bytes, err := json.Marshal(&entry)
	require.NoError(t, err)

	err = os.WriteFile(filepath.Join(root, currentEntry), bytes, os.FileMode(0640))
	require.NoError(t, err)

	db, err := Load(root, "v0")
	require.NoError(t, err)

	require.Equal(t, 3, db.Current.Ordinal)
	// can't use require.Equal with times
	require.True(t, entryTime.Equal(db.Current.Created))
}

func TestLoad_Entry_AutoRotate(t *testing.T) {
	root := t.TempDir()
	entryTime := time.Now().Add(-30 * time.Hour)

	entry := Entry{
		Ordinal: 3,
		Created: entryTime,
	}
	bytes, err := json.Marshal(&entry)
	require.NoError(t, err)

	err = os.WriteFile(filepath.Join(root, currentEntry), bytes, os.FileMode(0640))
	require.NoError(t, err)

	db, err := Load(root, "v0")
	require.NoError(t, err)

	require.Equal(t, 4, db.Current.Ordinal)
	require.WithinDuration(t, time.Now(), db.Current.Created, 5*time.Second)
}

func TestLoad_Todos(t *testing.T) {
	root := t.TempDir()

	todos := []string{
		"test1",
		"test2",
	}
	bytes, err := json.Marshal(&todos)
	require.NoError(t, err)

	err = os.WriteFile(filepath.Join(root, currentTodos), bytes, os.FileMode(0640))
	require.NoError(t, err)

	db, err := Load(root, "v0")
	require.NoError(t, err)

	require.Len(t, db.Todos, 2)
	require.Equal(t, "test1", db.Todos[0])
	require.Equal(t, "test2", db.Todos[1])
}

func TestDatabase_Rotate_Content(t *testing.T) {
	root := t.TempDir()
	db := Database{
		RootDir: root,
		Current: NewEntry(1),
	}

	expectedContent := []byte("test1")
	err := os.WriteFile(db.CurrentContentPath(), expectedContent, os.FileMode(0640))
	require.NoError(t, err)

	err = db.Rotate()
	require.NoError(t, err)

	_, err = os.Stat(db.CurrentContentPath())
	require.ErrorIs(t, err, os.ErrNotExist)

	files, err := filepath.Glob(filepath.Join(root, "*", "*", "*", "*.md"))
	require.NoError(t, err)

	require.Len(t, files, 1)

	actualContent, err := os.ReadFile(files[0])
	require.NoError(t, err)
	require.Equal(t, expectedContent, actualContent)
}

func TestDatabase_Rotate_Entry(t *testing.T) {
	root := t.TempDir()
	db := Database{
		RootDir: root,
		Current: NewEntry(1),
	}

	expectedCommands := []string{"test command1"}
	db.Current.Commands = expectedCommands
	db.Current.Ordinal = 3
	err := db.SaveCurrent("v0")
	require.NoError(t, err)

	err = db.Rotate()
	require.NoError(t, err)

	require.Equal(t, 4, db.Current.Ordinal)
	require.Empty(t, db.Current.Commands)

	files, err := filepath.Glob(filepath.Join(root, "*", "*", "*", "*.json"))
	require.NoError(t, err)

	require.Len(t, files, 1)

	bytes, err := os.ReadFile(files[0])
	require.NoError(t, err)

	var actualEntry Entry
	err = json.Unmarshal(bytes, &actualEntry)
	require.NoError(t, err)

	require.Equal(t, expectedCommands, actualEntry.Commands)
	require.Equal(t, 3, actualEntry.Ordinal)
}

func TestDuplicateRotateDoesntFail(t *testing.T) {
	root := t.TempDir()
	db := Database{
		RootDir: root,
		Current: NewEntry(1),
	}

	expectedCommands := []string{"test command1"}
	db.Current.Commands = expectedCommands
	db.Current.Ordinal = 3
	err := db.SaveCurrent("v0")
	require.NoError(t, err)

	err = db.Rotate()
	require.NoError(t, err)

	require.Equal(t, 4, db.Current.Ordinal)
	require.Empty(t, db.Current.Commands)

	require.NoError(t, db.Rotate())

	files, err := filepath.Glob(filepath.Join(root, "*", "*", "*", "*.json"))
	require.NoError(t, err)

	require.Len(t, files, 1)

	duplicateFiles, err := filepath.Glob(filepath.Join(root, "*", "*", "*", "*", "*.json"))
	require.NoError(t, err)

	require.Len(t, duplicateFiles, 1)
}
