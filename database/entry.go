package database

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"
)

type Entry struct {
	Ordinal        int
	Created        time.Time
	Commands       []string
	NewTodos       []string
	CompletedTodos []string
}

func NewEntry(ordinal int) *Entry {
	return &Entry{
		Created: time.Now(),
		Ordinal: ordinal,
	}
}

func (e *Entry) AddCommand(command string) bool {
	cmds, added := appendIfMissing(e.Commands, command)
	e.Commands = cmds
	return added
}

func (e Entry) Write(path string) error {
	bytes, err := json.MarshalIndent(e, "", "  ")
	if err != nil {
		return err
	}

	return os.WriteFile(path, bytes, os.FileMode(0640))
}

func (e Entry) String() string {
	result := &strings.Builder{}
	_, _ = fmt.Fprintf(result, "Entry #%d", e.Ordinal)
	if len(e.Commands) > 0 {
		_, _ = fmt.Fprintf(result, "\nCommands:")
		for _, cmd := range e.Commands {
			_, _ = fmt.Fprintf(result, "\n - `%s`", cmd)
		}
	}
	if len(e.CompletedTodos) > 0 {
		_, _ = fmt.Fprintf(result, "\nCompleted TODOs:")
		for _, todo := range e.CompletedTodos {
			_, _ = fmt.Fprintf(result, "\n - %s", todo)
		}
	}
	if len(e.NewTodos) > 0 {
		_, _ = fmt.Fprintf(result, "\nNew TODOs:")
		for _, todo := range e.NewTodos {
			_, _ = fmt.Fprintf(result, "\n - %s", todo)
		}
	}
	return result.String()
}

func loadEntry(path string) (*Entry, error) {
	b, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var current Entry
	err = json.Unmarshal(b, &current)
	return &current, err
}
