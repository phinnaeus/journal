package database

import (
	"encoding/json"
	"io/fs"
	"os"
	"path/filepath"
)

type Stats struct {
	NumEntries int
	NumContent int
}

type HistoryEntry struct {
}

type History map[string]*HistoryEntry

func (d Database) History(layout string) (History, error) {
	history := make(History)
	err := filepath.WalkDir(d.RootDir, func(path string, d fs.DirEntry, err error) error {
		if d.IsDir() || d.Name() != "entry.json" {
			return nil
		}

		bytes, err := os.ReadFile(path)
		if err != nil {
			return err
		}

		var entry Entry
		err = json.Unmarshal(bytes, &entry)
		if err != nil {
			return err
		}

		history[entry.Created.Format(layout)] = &HistoryEntry{}

		return nil
	})

	// add a history entry for today too
	history[d.Current.Created.Format(layout)] = &HistoryEntry{}
	return history, err
}

func (d Database) Stats() (Stats, error) {
	stats := Stats{}
	err := filepath.WalkDir(d.RootDir, func(path string, d fs.DirEntry, err error) error {
		if d.Name() == "entry.json" {
			stats.NumEntries += 1
		}
		if d.Name() == "content.md" {
			stats.NumContent += 1
		}
		//info, err2 := d.Info()
		//fmt.Printf("%s: %s (%s) (%d)\nmodtime: %s\nerr: %s\nerr:%s\n", path, d.Name(), d.Type(), info.Size(), info.ModTime(), err, err2)
		return nil
	})
	return stats, err
}
