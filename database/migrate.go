package database

import (
	log "github.com/sirupsen/logrus"
	"golang.org/x/mod/semver"
)

func migrate(oldVersion string, newVersion string) error {
	if semver.Compare(oldVersion, newVersion) == 0 {
		log.Debug("No database migration necessary.")
		return nil
	}

	return nil
}
