package database

import "strings"

func contains(slice []string, search string) bool {
	for _, e := range slice {
		if strings.EqualFold(e, search) {
			return true
		}
	}
	return false
}

func indexOf(slice []string, search string) int {
	for i, e := range slice {
		if strings.EqualFold(e, search) {
			return i
		}
	}
	return -1
}

func appendIfMissing(slice []string, new string) ([]string, bool) {
	if contains(slice, new) {
		return slice, false
	}
	return append(slice, new), true
}

func withoutIndex(slice []string, i int) []string {
	num := len(slice)
	if num == 1 {
		return []string{}
	}

	copy(slice[i:], slice[i+1:])
	slice[num-1] = ""
	return slice[:num-1]
}
