package database

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path/filepath"
)

func (d *Database) AddTodo(todo string) error {
	if contains(d.Todos, todo) {
		return errors.New("todo already exists")
	}

	d.Todos = append(d.Todos, todo)
	return nil
}

func (d *Database) CompleteTodo(i int) error {
	todo, err := d.removeTodo(i)
	if err != nil {
		return err
	}

	d.Current.CompletedTodos = append(d.Current.CompletedTodos, todo)
	return nil
}

func (d *Database) DeleteTodo(i int) error {
	todo, err := d.removeTodo(i)
	if err != nil {
		return err
	}

	newTodosIndex := indexOf(d.Current.NewTodos, todo)
	if newTodosIndex >= 0 {
		d.Current.NewTodos = withoutIndex(d.Current.NewTodos, newTodosIndex)
	} else {
		// TODO this should probably be an error and handled outside
		fmt.Println("WARN: TODO was added in a previous entry and is still present in the historical record.")
	}

	return nil
}

func (d *Database) removeTodo(i int) (string, error) {
	if i < 0 || i >= len(d.Todos) {
		return "", errors.New("TODO index out of bounds")
	}
	todo := d.Todos[i]
	d.Todos = withoutIndex(d.Todos, i)
	return todo, nil
}

func loadTodos(path string) ([]string, error) {
	var todos []string
	b, err := os.ReadFile(path)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return todos, nil
		}
		return nil, err
	}

	err = json.Unmarshal(b, &todos)
	if err != nil {
		return nil, err
	}

	return todos, nil
}

func (d Database) saveCurrentTodos() error {
	bytes, err := json.MarshalIndent(d.Todos, "", "  ")
	if err != nil {
		return err
	}

	return os.WriteFile(filepath.Join(d.RootDir, currentTodos), bytes, os.FileMode(0640))
}
