package io

import "os"

func FileExists(path string) bool {
	_, err := os.Stat(path)
	// TODO this is not totally correct
	return err == nil || !os.IsNotExist(err)
}
