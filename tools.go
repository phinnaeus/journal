//go:build tools

// just used to track tool dependencies, cannot be compiled
package main

import (
	_ "github.com/boumenot/gocover-cobertura"
	_ "gotest.tools/gotestsum"
)
