import React, {Component, SyntheticEvent} from "react";
import {DateTime} from "luxon";

type CalendarStatusProps = {
    selectedDate: DateTime,
    onDateChange: () => void,
}

type CalendarStatusState = Record<never, never>

export class CalendarStatus extends Component<CalendarStatusProps, CalendarStatusState> {
    constructor(props: CalendarStatusProps) {
        super(props);
    }

    render() {
        return (
            <div className={'calendar-status-container'}>
                <span className={'selected-date'}>Selected Date: {this.props.selectedDate.toLocaleString()}</span>
                <button className={'button'} onClick={this.props.onDateChange}>Today</button>
            </div>
        )
    }
}