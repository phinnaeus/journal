import React, {Component} from "react"
import {DateTime} from "luxon"

type ContentProps = {
    date: DateTime,
    hasContent: boolean,
}

type ContentState = {
    error?: Error,
    isLoaded?: boolean,
    content: string,
}

export class Content extends Component<ContentProps, ContentState> {
    constructor(props: ContentProps) {
        super(props)
        this.state = {
            isLoaded: false,
            content: "",
        }
    }

    fetchContent(date: DateTime) {
        fetch(`/api/content/${date.toISODate()}`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(response.statusText)
                }
                return response.text()
            })
            .then(
                (content) => {
                    this.setState({
                        isLoaded: true,
                        content: content,
                        error: undefined,
                    })
                },
                (err) => {
                    this.setState({
                        isLoaded: true,
                        error: err,
                    })
                }
            )
    }

    componentDidMount() {
        if (this.props.hasContent) {
            this.fetchContent(this.props.date)
        }
    }

    componentDidUpdate(prevProps: ContentProps) {
        const d1 = this.props.date
        const d2 = prevProps.date
        if (d1.hasSame(d2, 'day') && this.props.hasContent == prevProps.hasContent) {
            return
        }
        this.fetchContent(this.props.date)
    }

    render() {
        if (!this.state.isLoaded || this.state.error) {
            let msg = 'Not yet loaded...'
            if (this.state.error) {
                msg = this.state.error.message
            }
            return (
                <div className={'content-container'}><p>{msg}</p></div>
            )
        }
        return (
            <div className={'content-container'} dangerouslySetInnerHTML={ {__html: this.state.content} } />
        )
    }
}