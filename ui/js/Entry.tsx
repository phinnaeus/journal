import React, {Component} from "react";
import {DateTime} from "luxon";

type EntryProps = {
    date: DateTime,
    hasContent: boolean,
}

type EntryState = {
    error?: Error,
    isLoaded?: boolean,
    entry: EntryType,
}

// This should mirror the Entry type in database/entry.go
type EntryType = {
    Ordinal?: number,
    Created?: DateTime,
    Commands?: Array<string>,
    NewTodos?: Array<string>,
    CompletedTodos?: Array<string>,
}

// TODO superclass for dynamically loaded component
export class Entry extends Component<EntryProps, EntryState> {
    constructor(props: EntryProps) {
        super(props)
        this.state = {
            isLoaded: false,
            entry: {},
        }
    }

    fetchEntry(date: DateTime) {
        fetch(`/api/entry/${date.toISODate()}`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(response.statusText)
                }
                return response.text()
            })
            .then(
                (entry) => {
                    this.setState({
                        isLoaded: true,
                        entry: JSON.parse(entry),
                        error: undefined,
                    })
                },
                (err) => {
                    this.setState({
                        isLoaded: true,
                        error: err,
                    })
                }
            )
    }

    componentDidMount() {
        if (this.props.hasContent) {
            this.fetchEntry(this.props.date)
        }
    }

    componentDidUpdate(prevProps: EntryProps) {
        const d1 = this.props.date
        const d2 = prevProps.date
        if (d1.hasSame(d2, 'day') && this.props.hasContent == prevProps.hasContent) {
            return
        }
        this.fetchEntry(this.props.date)
    }
    
    render() {
        const e = this.state.entry
        const commands = (e.Commands || []).map((c) => <li>{c}</li>)
        const newTodos = (e.NewTodos || []).map((t) => <li>{t}</li>)
        const completedTodos = (e.CompletedTodos || []).map((t) => <li>{t}</li>)
        return (
            <div className={'entry-container'}>
                <h1>Entry #{e.Ordinal}</h1>
                <h2>Created {e.Created?.toLocaleString()}</h2>
                <h3>Commands:</h3>
                <ul>{commands}</ul>
                <h3>New To-Do:</h3>
                <ul>{newTodos}</ul>
                <h3>Completed To-Do:</h3>
                <ul>{completedTodos}</ul>
            </div>
        )
    }
}