import React, {Component} from "react"
import {DateTime} from "luxon"
import Calendar, {PerDateMetadata} from "@phinnaeus/react-calendar"
import {Content} from "./Content"
import {CalendarStatus} from "./CalendarStatus";
import {Entry} from "./Entry";

type JournalProps = Record<never, never>

type JournalState = {
    selectedDate: DateTime,
}

const calendarClasses = new Map<string, string>([
    ['calendar-container', 'calendar-container'],
    ['month-container', 'month-container'],
    ['day', 'day'],
    ['selected-day', 'selected']
])

export class Journal extends Component<JournalProps, JournalState> {
    private readonly today: DateTime
    private readonly dateEntries: PerDateMetadata
    constructor(props: JournalProps) {
        super(props)

        this.today = DateTime.now()

        const dateEntries = new PerDateMetadata()
        const historyElement = document.querySelector('#history')
        if (historyElement && historyElement.textContent) {
            const historyEntries = JSON.parse(historyElement.textContent) || []
            Object.entries(historyEntries).forEach(([k, _]) => {
                dateEntries.add(k , 'entry')
            })
            dateEntries.add(this.today.toISODate(), 'today')
        } else {
            console.error('no history element found')
        }
        this.dateEntries = dateEntries

        this.state = {
            selectedDate: this.today,
        }
        this.onDateChange = this.onDateChange.bind(this)
        this.resetDate = this.resetDate.bind(this)
    }

    onDateChange(date: DateTime) {
        if (this.state.selectedDate.equals(date)) {
            console.log('ignoring same date')
        }
        if (this.dateEntries.has(date)) {
            this.setState({
                selectedDate: date,
            })

        } else {
            console.log('ignoring date without an entry')
        }
    }

    resetDate() {
        this.onDateChange(this.today)
    }

    render() {
        // console.log('big render', this.dateEntries)
        const hasContent = this.dateEntries.get(this.state.selectedDate).includes('entry')
        return (
            <div className={'journal-container'}>
                <div className={'content-row-container'}>
                    <Content date={this.state.selectedDate} hasContent={hasContent} />
                    {/*TODO: to-dos*/}
                    <Entry date={this.state.selectedDate} hasContent={hasContent} />
                </div>
                <CalendarStatus selectedDate={this.state.selectedDate} onDateChange={this.resetDate} />
                <Calendar date={this.today.plus({ months: 1 })}
                          selectedDate={this.state.selectedDate}
                          firstWeekDay={1}
                          onDateSelect={this.onDateChange}
                          dateClasses={this.dateEntries}
                          customClasses={calendarClasses}
                          customWeekdayNames={['S', 'M', 'T', 'W', 'T', 'F', 'S']}
                />
            </div>
        )
    }
}