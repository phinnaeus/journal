import ReactDOM from "react-dom"
import React from "react"

import {Journal} from './Journal'

ReactDOM.render(
    <Journal />,
    document.querySelector('#journal')
)

// scroll the calendar all the way over on page load
const cal = document.querySelector('.calendar-container')
if (cal) {
    cal.scrollTo(cal.scrollWidth, 0)
}