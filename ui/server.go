package ui

import (
	"embed"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gomarkdown/markdown"
	log "github.com/sirupsen/logrus"
	"html/template"
	"io/fs"
	"net/http"
	"os"
	"path/filepath"
	"phinnae.us/journal/database"
	"time"
)

const isoLayout = "2006-01-02"

//go:embed templates dist
var embededFiles embed.FS

func Serve(db database.Database) error {
	history, err := db.History(isoLayout)
	if err != nil {
		return err
	}

	gin.SetMode(gin.ReleaseMode)
	//gin.DefaultWriter = log.Writer()
	root := gin.Default()

	err = addStaticRoutes(root)
	if err != nil {
		return err
	}

	err = addIndexRoute(root, history)
	if err != nil {
		return err
	}

	// api
	api := root.Group("/api")
	{
		// TODO paginate history
		api.GET("/history", func(c *gin.Context) {
			c.JSON(http.StatusOK, history)
		})

		api.GET("/entry/:date", func(c *gin.Context) {
			var entry *database.Entry
			dateParam := c.Param("date")
			entryTime, err := time.Parse(isoLayout, dateParam)
			if err != nil {
				c.AbortWithError(http.StatusInternalServerError, err)
				return
			}

			if dateParam == db.Current.Created.Format(isoLayout) {
				log.Debug("current entry requested")
				entry = db.Current
			} else {
				log.Debugf("entry requested for %s", dateParam)
				entry, err = db.LoadEntry(entryTime)
				if err != nil {
					c.AbortWithError(http.StatusNotFound, err)
					return
				}
				// past days shouldn't change, so we can cache them
				c.Header("Cache-Control", "private, max-age=86400")
			}

			c.JSON(http.StatusOK, entry)
		})

		api.GET("/content/:date", func(c *gin.Context) {
			var content []byte
			dateParam := c.Param("date")
			entryTime, err := time.Parse(isoLayout, dateParam)
			if err != nil {
				c.AbortWithError(http.StatusInternalServerError, err)
				return
			}

			if dateParam == db.Current.Created.Format(isoLayout) {
				log.Debug("current content requested")
				content, err = os.ReadFile(db.CurrentContentPath())
				if err != nil {
					c.AbortWithError(http.StatusNotFound, err)
					return
				}
			} else {
				log.Debugf("content requested for %s", dateParam)
				content, err = db.LoadContent(entryTime)
				if err != nil {
					c.AbortWithError(http.StatusNotFound, err)
					return
				}
				// past days shouldn't change, so we can cache them
				c.Header("Cache-Control", "private, max-age=86400")
			}

			c.Data(http.StatusOK, "text/html", markdown.ToHTML(content, nil, nil))
		})

		api.GET("/todos", func(c *gin.Context) {
			c.JSON(http.StatusOK, db.Todos)
		})
	}

	// TODO this prints too soon
	fmt.Println("Starting server at http://localhost:8080/")
	return root.Run("localhost:8080")
}

func addStaticRoutes(root *gin.Engine) error {
	staticFs, err := fs.Sub(embededFiles, "dist")
	if err != nil {
		return err
	}
	root.StaticFS("/static", http.FS(staticFs))

	// Can't use `root.StaticFile()` as it doesn't accept a custom FS
	const favicon = "favicon.png"
	handler := func(c *gin.Context) {
		c.Header("Cache-Control", "public, max-age=86400")
		c.FileFromFS(filepath.Join("templates", favicon), http.FS(embededFiles))
	}
	root.GET(favicon, handler)
	root.HEAD(favicon, handler)

	return nil
}

func addIndexRoute(root *gin.Engine, history database.History) error {
	indexTemplate, err := template.ParseFS(embededFiles, filepath.Join("templates", "index.html"))
	if err != nil {
		return err
	}
	root.SetHTMLTemplate(indexTemplate)

	historyBytes, err := json.Marshal(history)
	if err != nil {
		return err
	}

	handler := func(c *gin.Context) {
		c.HTML(http.StatusOK, "", gin.H{
			"title":       "test",
			"historyJson": template.JS(historyBytes),
		})
	}
	root.GET("/", handler)
	root.GET("/index.html", handler)

	return nil
}
