// Originally from https://github.com/creativeprojects/go-selfupdate/blob/7b16587034c3587376a358cd198a9301c81f2af2/update/apply_test.go

package updater

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

var (
	oldFile = []byte{0xDE, 0xAD, 0xBE, 0xEF}
	newFile = []byte{0x01, 0x02, 0x03, 0x04, 0x05, 0x06}
)

func cleanup(path string) {
	os.Remove(path)
	os.Remove(fmt.Sprintf(".%s.new", path))
}

// we write with a separate name for each test so that we can run them in parallel
func writeOldFile(path string, t *testing.T) {
	if err := ioutil.WriteFile(path, oldFile, 0777); err != nil {
		t.Fatalf("Failed to write file for testing preparation: %v", err)
	}
}

func validateUpdate(path string, err error, t *testing.T) {
	if err != nil {
		t.Fatalf("Failed to update: %v", err)
	}

	buf, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatalf("Failed to read file post-update: %v", err)
	}

	if !bytes.Equal(buf, newFile) {
		t.Fatalf("File was not updated! Bytes read: %v, Bytes expected: %v", buf, newFile)
	}
}

func TestApplySimple(t *testing.T) {
	fName := "TestApplySimple"
	defer cleanup(fName)
	writeOldFile(fName, t)

	err := Apply(bytes.NewReader(newFile), Options{
		TargetPath: fName,
	})
	validateUpdate(fName, err, t)
}

func TestApplyOldSavePath(t *testing.T) {
	fName := "TestApplyOldSavePath"
	defer cleanup(fName)
	writeOldFile(fName, t)

	oldfName := "OldSavePath"

	err := Apply(bytes.NewReader(newFile), Options{
		TargetPath:  fName,
		OldSavePath: oldfName,
	})
	validateUpdate(fName, err, t)

	if _, err := os.Stat(oldfName); os.IsNotExist(err) {
		t.Fatalf("Failed to find the old file: %v", err)
	}

	cleanup(oldfName)
}

func TestWriteError(t *testing.T) {
	fName := "TestWriteError"
	defer cleanup(fName)
	writeOldFile(fName, t)

	openFile = func(name string, flags int, perm os.FileMode) (*os.File, error) {
		f, err := os.OpenFile(name, flags, perm)

		// simulate Write() error by closing the file prematurely
		f.Close()

		return f, err
	}
	defer func() {
		openFile = os.OpenFile
	}()

	err := Apply(bytes.NewReader(newFile), Options{TargetPath: fName})
	if err == nil {
		t.Fatalf("Allowed an update to an empty file")
	}
}
