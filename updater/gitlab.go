package updater

import "time"

type gitlabRelease struct {
	Name        string    `json:"name"`
	TagName     string    `json:"tag_name"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	ReleasedAt  time.Time `json:"released_at"`
	Author      struct {
		Id        int    `json:"id"`
		Name      string `json:"name"`
		Username  string `json:"username"`
		State     string `json:"state"`
		AvatarUrl string `json:"avatar_url"`
		WebUrl    string `json:"web_url"`
	} `json:"author"`
	Commit struct {
		Id             string    `json:"id"`
		ShortId        string    `json:"short_id"`
		CreatedAt      time.Time `json:"created_at"`
		ParentIds      []string  `json:"parent_ids"`
		Title          string    `json:"title"`
		Message        string    `json:"message"`
		AuthorName     string    `json:"author_name"`
		AuthorEmail    string    `json:"author_email"`
		AuthoredDate   time.Time `json:"authored_date"`
		CommitterName  string    `json:"committer_name"`
		CommitterEmail string    `json:"committer_email"`
		CommittedDate  time.Time `json:"committed_date"`
		Trailers       struct {
		} `json:"trailers"`
		WebUrl string `json:"web_url"`
	} `json:"commit"`
	UpcomingRelease bool   `json:"upcoming_release"`
	CommitPath      string `json:"commit_path"`
	TagPath         string `json:"tag_path"`
	Assets          struct {
		Count   int `json:"count"`
		Sources []struct {
			Format string `json:"format"`
			Url    string `json:"url"`
		} `json:"sources"`
		Links []assetLinks `json:"links"`
	} `json:"assets"`
	Evidences []struct {
		Sha         string    `json:"sha"`
		Filepath    string    `json:"filepath"`
		CollectedAt time.Time `json:"collected_at"`
	} `json:"evidences"`
	Links struct {
		Self    string `json:"self"`
		EditUrl string `json:"edit_url"`
	} `json:"_links"`
}

type assetLinks struct {
	Id             int    `json:"id"`
	Name           string `json:"name"`
	Url            string `json:"url"`
	DirectAssetUrl string `json:"direct_asset_url"`
	External       bool   `json:"external"`
	LinkType       string `json:"link_type"`
}
