package updater

import (
	"encoding/json"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"golang.org/x/mod/semver"
	"io"
	"io/ioutil"
	"net/http"
	"runtime"
	"strings"
)

const gitlab = "https://gitlab.com/api/v4/projects/28990486/"

var (
	osArchDash  = fmt.Sprintf("%s-%s", runtime.GOOS, runtime.GOARCH)
	osArchUnder = fmt.Sprintf("%s_%s", runtime.GOOS, runtime.GOARCH)
)

type release struct {
	version  string
	location string
	sha      string
}

func Update(currentVersion string, force bool, explicit string) error {
	if !semver.IsValid(currentVersion) {
		return fmt.Errorf("%q is not a valid semver."+
			"i'd tell you why but semver doesn't expose their error messages for some reason", currentVersion)
	}

	log.Debugf("Checking update for version %q / %q", currentVersion, semver.Canonical(currentVersion))

	if !force && semver.Prerelease(currentVersion) != "" {
		log.Warnf("not updating pre-release version without force")
		return nil
	}

	var r *release
	var err error
	if explicit == "" {
		r, err = getLatestVersion()
		if err != nil {
			return err
		}
		log.Debugf("found latest version in GitLab: %q / %q", r.version, semver.Canonical(r.version))
	} else {
		r, err = getSpecificVersion(explicit)
	}

	if r.location == "" {
		return errors.New("download location was missing from gitlab release")
	}

	if force || semver.Compare(currentVersion, r.version) < 0 {
		log.Infof("Updating to %q", r.version)
		log.Debugf("Found new version located at %s", r.location)
		reader, size, err := getUpdateFile(r.location)
		if err != nil {
			return err
		}
		defer reader.Close()
		// TODO this should be dynamic exe place
		return Apply(reader, Options{
			TargetPath:  "journal",
			OldSavePath: "journal.old",
			Size:        size,
		})
	}

	log.Info("No update required.")
	return nil
}

func getUpdateFile(location string) (io.ReadCloser, int64, error) {
	req, err := http.NewRequest("GET", location, nil)
	if err != nil {
		return nil, 0, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, 0, err
	}
	return resp.Body, resp.ContentLength, nil
}

func getAllReleases() ([]gitlabRelease, error) {
	var r []gitlabRelease
	req, err := http.NewRequest("GET", gitlab+"releases", nil)
	if err != nil {
		return r, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return r, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return r, err
	}

	err = json.Unmarshal(body, &r)
	if err != nil {
		return r, err
	}

	if len(r) == 0 {
		return r, errors.New("no releases found")
	}
	return r, nil
}

func getSpecificVersion(explicit string) (*release, error) {
	releases, err := getAllReleases()
	if err != nil {
		return nil, err
	}

	for _, rel := range releases {
		if explicit == rel.TagName {
			return &release{
				version:  rel.TagName,
				location: getCorrectDownload(rel.Assets.Links),
				sha:      "", // TODO
			}, nil
		}
	}
	return nil, fmt.Errorf("no release found for %q", explicit)
}

func getLatestVersion() (*release, error) {
	releases, err := getAllReleases()
	if err != nil {
		return nil, err
	}

	return &release{
		version:  releases[0].TagName,
		location: getCorrectDownload(releases[0].Assets.Links),
		sha:      "", // TODO
	}, nil
}

func getCorrectDownload(links []assetLinks) string {
	log.Debugf("looking for artifacts for %s", osArchUnder)
	for _, link := range links {
		log.Debugf("checking link %q", link.Name)
		if strings.HasSuffix(link.Name, osArchUnder) || strings.HasSuffix(link.Name, osArchDash) {
			log.Debugf("found artifacts: %q", link.Name)
			return link.DirectAssetUrl
		}
	}
	return ""
}
